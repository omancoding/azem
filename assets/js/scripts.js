function scroll_to_class(element_class, removed_height) {
    var scroll_to = $(element_class).offset().top - removed_height;
    if ($(window).scrollTop() != scroll_to) {
        $('html, body').stop().animate({
            scrollTop: scroll_to
        }, 0);
    }
}

function bar_progress(progress_line_object, direction) {
    var number_of_steps = progress_line_object.data('number-of-steps');
    var now_value = progress_line_object.data('now-value');
    var new_value = 0;
    if (direction == 'right') {
        new_value = now_value + (100 / number_of_steps);
    } else if (direction == 'left') {
        new_value = now_value - (100 / number_of_steps);
    }
    progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
}

jQuery(document).ready(function() {

    /*
        Fullscreen background
    */
    $.backstretch("assets/img/backgrounds/1.jpg");

    $('#top-navbar-1').on('shown.bs.collapse', function() {
        $.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function() {
        $.backstretch("resize");
    });

    /*
        Form
    */
    $('.f1 fieldset:first').fadeIn('slow');

    $('.f1 input[type="text"], .f1 input[type="password"], .f1 textarea').on('focus', function() {
        $(this).removeClass('input-error');
    });

    // next step
    $('.f1 .btn-next').on('click', function() {

        //form validation
        var errorFlag = false;
        $('.inputError').remove();
        $('fieldset[style*="display: block"]').find('textarea, input').each(function() {
            //console.log($(this).attr('name'), $(this).val());

            // This filters the mobile subscription checking if there are providers for that service
            if ($(this).closest('#notifyarea').css('display')=='none') {
                return;
            }

            if ($(this).val() == "") {
                console.log($(this));
                $(this).addClass('input-error');
                errorFlag = true;
                $(this).parent().append('<small class="inputError text-danger"> البيانات ناقصة </small>')
            } else {
                $(this).removeClass('input-error');
            }
        })

        if (errorFlag) {
            console.log('البيانات ناقصة')
            return;
        }
        var parent_fieldset = $(this).parents('fieldset');
        var next_step = true;
        // navigation steps / progress steps
        var current_active_step = $(this).parents('.f1').find('.f1-step.active');
        var progress_line = $(this).parents('.f1').find('.f1-progress-line');

        // fields validation
        /*parent_fieldset.find('input[type="text"], input[type="password"], textarea').each(function() {
            if( $(this).val() == "" ) {
                $(this).addClass('input-error');
                next_step = false;
            }
            else {
                $(this).removeClass('input-error');
            }
        });*/
        // fields validation


        if (next_step) {
            parent_fieldset.fadeOut(400, function() {
                // change icons
                current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                // progress bar
                bar_progress(progress_line, 'right');
                // show next step
                $(this).next().fadeIn(
                    function() {
                        $($(this).find('input')[0]).focus();
                    });
                // scroll window to beginning of the form
                scroll_to_class($('.f1'), 20);
            });
        }

    });

    // previous step
    $('.f1 .btn-previous').on('click', function() {
        // navigation steps / progress steps
        var current_active_step = $(this).parents('.f1').find('.f1-step.active');
        var progress_line = $(this).parents('.f1').find('.f1-progress-line');

        $(this).parents('fieldset').fadeOut(400, function() {
            // change icons
            current_active_step.removeClass('active').prev().removeClass('activated').addClass('active');
            // progress bar
            bar_progress(progress_line, 'left');
            // show previous step
            $(this).prev().fadeIn();
            // scroll window to beginning of the form
            scroll_to_class($('.f1'), 20);
        });
    });

    var data_path;
    $('button[type="submit"]').click(function() {
        data_path = $(this).attr('data-path');
    })
    // submit
    $('.f1').on('submit', function(e) {
        e.preventDefault();
        var data = {}
        // fields validation
        var errorFlag = false;
        $('.inputError').remove();
        $(this).find('textarea, input').each(function() {
            //console.log($(this).attr('name'), $(this).val());
            if ($(this).val() == "") {
                $(this).addClass('input-error');
                errorFlag = true;
                $(this).parent().append('<small class="inputError text-danger"> البيانات ناقصة </small>')
            } else {
                $(this).removeClass('input-error');
            }
            data[$(this).attr('name')] = $(this).val();
        });

        if (errorFlag) {
            console.log('cannot')
            return false;
        }


        $.ajax({
            type: "POST",
            url: data_path,
            data: data,
            success: success,
        });

        function success(status) {
            window.location.replace(status)
        }

        // fields validation

    });

    //$('.f1 .btn-next')[0].click();
    //$('.f1 .btn-next')[1].click();
    //setTimeout(function() {$('.f1 .btn-next')[1].click();},1000);



});

//provider 
$('.round-icon-menu li').on('click', pf1li_click);

function pf1li_click(e) {
    // remove from other classes;
    $('.round-icon-menu li').removeClass('active');
    // color the selection (active);
    $(this).addClass('active')
    // show the write about me.
    $('#pf1-about-me').css('display', 'block');
    // replace the label text;
    $('#pf1-about-me label').text(placeholders[$(this).attr('id')]);
    // focus the textarea
    $('#pf1-about-me textarea').focus();
    // record the selection in the hidden input 
    $('input[name="pf1_service"]').val($(this).attr('id'))

}
var placeholders = {
    'pf1auto': 'لدي خبرة في مجال تصليح السيارات اليابانية. نُقدم خدمة التوصيل لخةمتي صيانة و تصليح السيارات.',
    'pf1plu': 'خبير في صيانة و تركيب الأنابيب النحاسية و البلاستيك. العمل مضمون لستة أشهر.',
    'pf1ele': 'كهربائي'
}
// END provider first

//provider second
$('.list-group li').on('click', pf2li_click);



function pf2li_click(e) {
    // remove from other classes;
    //$('.list-group li').removeClass('active');
    // color the selection (active);
    $(this).addClass('active')

}
// END provider second


//service
var providers;
$('.round-icon-menu-service li').on('click', sf1li_click);

function sf1li_click(e) {
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    $('input[name="sf1_service"]').val($(this).attr('id'));

    // prepare the available regions;
    $('#serviceAreaSelect').empty();
    var regions = providers[$(this).attr('id')];
    for (var region in regions) {
        //console.log(region);
        $('#serviceAreaSelect').append($('<li></li>').addClass('list-group-item').text(region))
    }

    $('.list-group li').on('click', sf2li_click);

    function sf2li_click(e) {
        $('.list-group li').removeClass('active');
        $(this).addClass('active');

        $('#cards').empty();
        for (var card of regions[$(this).text()]) {
            $('#cards').append(createCard(card.name, card.servicedesc, card.mobile))
        }

        $('.f1 .btn-next')[1].click();

    }

    // allow users to subscribe interest in service
    if (!regions) {
        console.log(regions)
        $('#notifyarea').css('display', 'block');
        $('#serviceAreaTitle').text('متوسعون في الخدمة. هل تود أن تصلك إشعارات الخدمة عندما تأتي إلى منطقتك؟')
    }
    $('.f1 .btn-next')[0].click();

}


$.ajax({
    type: "GET",
    url: '/service/providers',
    success: getSuccess,
});

function getSuccess(data) {
    console.log(data);
    providers = data;
}

function createCard(title, text, mobile) {
    var top = $("<div></div>").addClass('card text-right');
    var child = $("<div></div>").addClass('card-body');
    top.append(child);

    var wstext = encodeURIComponent('شكراً لإختيارك عزم \n https://azem.work \n\n للتواصل مع *'+title+'* الرجاء الضغط على https://wa.me/968'+mobile+' \n\n يمكنك أيضا تقييم الخدمة المقدمة بالرد على هذه الرسالة.')
    var wslink = 'https://wa.me/96895972220?text='+wstext;

    var header = $("<h5> </h5>").addClass('card-title').text(title).css({ color: 'black', 'font-size': '20px' });
    var p = $("<p> </p>").addClass('card-text').text(text);
    var whatsapp = $("<a target='_blank' href="+wslink+"> <i class='fab fa-whatsapp-square'> </i> </a>").css({ 'color': 'green', 'font-size': '40px' });
    var phone = $("<a href=tel:968"+mobile+"> <i class='fas fa-phone-square'> </i> </a>").css({ 'color': 'lightblue', 'font-size': '40px' });

    child.append(header, p, whatsapp, phone)

    return top;
}