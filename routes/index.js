var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/service')
});

router.get('/success', function(req, res, next) {
	var title = req.query.title;
	var content = req.query.content;
	res.render('success', {title: title, content: content })
});

router.get('/failure', function(req, res, next) {
	var title = req.query.title;
	var content = req.query.content;
	res.render('failure', {title: title, content: content })
});


router.get('/about', function(req, res, next) {
	res.render('about/main');
});



module.exports = router;
