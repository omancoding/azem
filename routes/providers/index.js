var express = require('express');
var url = require('url');
var router = express.Router();
var data = require('../models')

/* GET home page. */
router.get('/', function(req, res, next) {
		res.render('providers/new')
});

// make new provider request
router.post('/new', function(req, res, next) {
		var service = req.body.pf1_service;
		var serviceDesc = req.body.pf1_servicedesc;
		var areaF = req.body.pf2_area1;
		var areaS = req.body.pf2_area2;
		var name = req.body.pf3_fullname;
		var mobile = req.body.pf3_mobile;

		var provider = new data.providers({
				name: name,
				service: service,
				servicedesc: serviceDesc,
				areaF: areaF,
				areaS: areaS,
				mobile: mobile,
		})

		var viewData = {
				title: 'تم التسجيل',
				content: 'سيتم التواصل معك ' + name + ' لتأكيد تسجيلك.'
		}

		provider.save(handlePostResponce(res, viewData));

});

function handlePostResponce(res, viewData) {
		return function(err) {
				if (err) {
						console.log('Error on save!', err);
						if (err.code == '11000') {
								var urlFormat = url.format({
										pathname: "/failure",
										query: {
												"title": 'خطأ في التسجيل',
												"content": 'الرقم مُسجل سابقاً'
										}
								})
								res.send(urlFormat);
						}
				} else {
						var urlFormat = url.format({
								pathname: "/success",
								query: {
										"title": viewData.title,
										"content": viewData.content
								}
						})
						res.send(urlFormat);
				}
		}
}


module.exports = router;