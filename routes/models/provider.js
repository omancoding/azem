var mongoose = require ('mongoose');

var providerSchema = new mongoose.Schema({
  name: { type: String, trim: true },
  service: { type: String, trim: true },
  servicedesc: { type: String, trim: true },
  mobile: {type: String, unique: true},
  areaF: {type: String},
  areaS: {type: String},
  date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('providers', providerSchema);