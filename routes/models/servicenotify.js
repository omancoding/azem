var mongoose = require ('mongoose');

var serviceNotifySchema = new mongoose.Schema({
  service: { type: String, trim: true },
  mobile: {type: String/*, unique: true*/},
  area: { type: String, trim: true },
  type: { type: String, trim: true },
  date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('serviceNotify', serviceNotifySchema);