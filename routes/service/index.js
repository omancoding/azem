var express = require('express');
var url = require('url');
var router = express.Router();
var data = require('../models')

router.get('/', async function(req, res, next) {
	res.render('service/new');
});

router.get('/providers', async function(req, res, next) {
	var providers = await getAllProvidersPromise();
	res.json(groupByMulti(providers, 'service', 'areaF'));
});

router.post('/notify/area', function(req, res, next) {
  var service = req.body.sf1_service;
  var area = req.body.sf2_areainterest;
  var mobile = req.body.sf2_mobileinterest ;

  var recipient = new data.servicenotify({
  	service: service,
  	area: area,
  	mobile: mobile,
  	type: 'serviceNewInArea'
  });

  var viewData = {
  	title: 'تم الاشتراك',
  	content: 'سيتم تنبيهك برسالة نصية في حال توفر خدمة في '+area
  }

  recipient.save(handlePostResponce(res, viewData));

});

function handlePostResponce(res,viewData) {
	return function(err) {
		if (err) {
			console.log('Error on save!', err)
		} else {
			var urlFormat = url.format({
       pathname:"/success",
       query: {
          "title": viewData.title,
          "content": viewData.content
        }
     })
			res.send(urlFormat);
		}
	}
}

function getAllProvidersPromise() {
	return new Promise(resolve => {
		data.providers.find({}).exec(function(err, result) {
			if (!err) {
				// handle result
				resolve(result);
			} else {
				// error handling
			};
		});
	})
}

function groupBy(objectArray, property) {
	return objectArray.reduce(function(acc, obj) {
		var key = obj[property];
		if (!acc[key]) {
			acc[key] = [];
		}
		acc[key].push(obj);
		return acc;
	}, {});
}

function groupByMulti(objectArray, property, subproperty) {
	return objectArray.reduce(function(acc, obj) {
		// This will prevent creation of accumulated object that
		// has undefined keys.
		if (property == undefined || subproperty == undefined) {
			return acc;
		}
		var key = obj[property];
		var subkey = obj[subproperty];
		if (!acc[key]) {
			acc[key] = {};
		}
		if (!acc[key][subkey]) {
			acc[key][subkey] = [];
		}
		acc[key][subkey].push(obj);
		return acc;
	}, {});
}


module.exports = router;